import 'dart:convert';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello Bear',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: MyHomePage(
        title: 'Hello Bear'
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, @required this.title})
      : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  WebSocketChannel channel;
  Image imgBear;
  Image imgHello;
  Image imgCurrent;
  bool status = true;

  @override
  void initState() {
    channel = IOWebSocketChannel.connect("ws://pm.tada.team/ws");

    imgBear = Image.asset('assets/bear.png');
    imgHello = Image.asset('assets/hello.png');
    channel.stream.listen((data) => setState(() {
      status = json.decode(data)["state"] == 1;
      if (status) {
        imgCurrent = imgBear;
      } else {
        imgCurrent = imgHello;
      };
    }));
    super.initState();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(imgBear.image, context);
    precacheImage(imgHello.image, context);
  }
  void changeStatus() {
    setState(() {
      if (!status) {
        channel.sink.add('{"state": 1}');
      } else {
        channel.sink.add('{"state": 0}');
      }
    });
  }
  @override
  void dispose() {
    channel.sink.close();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: GestureDetector(
        onTap: changeStatus,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Container(
                  padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
                  child: Image.asset(
                    status ? 'assets/bear.png' : 'assets/hello.png'),
                )
              ),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                  child: Text(
                    status ? 'MEDVED' : 'PRIVED',
                    style: new TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}
